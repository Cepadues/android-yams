package com.example.androslacompote;

import java.util.Arrays;

public class YamsDice {
    public Dice[] dice5 = new Dice[5];
    private int[] NumberOf = new int[6];

    YamsDice() {
        for (int i = 0; i < 5; i++) {
            dice5[i] = new Dice();
        }
        countNumber();
    }

    private void countNumber() {
        Arrays.fill(NumberOf, 0);
        for (int i = 0; i < 5; i++) {
            NumberOf[dice5[i].val_dice-1]++;
        }
    }

    public void rollYams(String position) {
        int str_length = position.length();
        if (!position.isEmpty()) { // return true if String is empty
            for (int i = 0; i < str_length; i++) {
                int ascii_char = position.charAt(i) - '0';
                if (ascii_char >= 1 && ascii_char <= 5) {
                    this.dice5[ascii_char - 1].roll();
                }
            }
        }
        countNumber();
    }

    public int sumOfDice() {
        int somme = 0;
        for (int i = 0; i < 5; i++) {
            somme += this.dice5[i].val_dice;
        }
        return somme;
    }

    public int sumOfVal(int val) {
        return this.NumberOf[val-1] * val;
    }

    public int checkBrelan() {
        int score = 0;
        for (int i = 0; i < 6; i++) {
            score = this.NumberOf[i] >= 3 ? (i + 1) * 3 : score;
        } // The ? operator allow to do a if else statement,
        // if NumberOf[i] >= 3 => score = (i+1) * 3 else score = score
        return score;
    }

    public int checkFull() {
        int score = 0;
        for (int i = 0; i < 6; i++) {
            score = this.NumberOf[i] == 2 ? 1 : score;
        }
        return (score <= 0 || checkBrelan() <= 0) ? 0 : 25;
        // return (score > 0 && checkBrelan() > 0) ? 25 : 0;
    }

    public int checkCarre() {
        int score = 0;
        for (int i = 0; i < 6; i++) {
            score = this.NumberOf[i] >= 4 ? (i + 1) * 4 : score;
        }
        return score;
    }

    public int checkYams() {
        int score = 0;
        for (int i = 0; i < 6; i++) {
            score = this.NumberOf[i] >= 5 ? 50 : score;
        }
        return score;
    }

    public int checkPetiteSuite() {
        boolean PS1 = NumberOf[0] != 0 && NumberOf[1] != 0 && NumberOf[2] != 0 && NumberOf[3] != 0;
        boolean PS2 = NumberOf[1] != 0 && NumberOf[2] != 0 && NumberOf[3] != 0 && NumberOf[4] != 0;
        boolean PS3 = NumberOf[2] != 0 && NumberOf[3] != 0 && NumberOf[4] != 0 && NumberOf[5] != 0;
        return (PS1 || PS2 || PS3) ? 30 : 0;
    }

    public int checkGrandeSuite() {
        boolean GS1 = NumberOf[0] != 0 && NumberOf[1] != 0 && NumberOf[2] != 0 && NumberOf[3] != 0 && NumberOf[4] != 0;
        boolean GS2 = NumberOf[1] != 0 && NumberOf[2] != 0 && NumberOf[3] != 0 && NumberOf[4] != 0 && NumberOf[5] != 0;
        return (GS1 || GS2) ? 40 : 0;
    }

    public int getScore(int index) {
        switch (index) {
            case 0:
                return sumOfVal(1);
            case 1:
                return sumOfVal(2);
            case 2:
                return sumOfVal(3);
            case 3:
                return sumOfVal(4);
            case 4:
                return sumOfVal(5);
            case 5:
                return sumOfVal(6);
            case 6:
                return checkBrelan();
            case 7:
                return checkFull();
            case 8:
                return checkCarre();
            case 9:
                return checkPetiteSuite();
            case 10:
                return checkGrandeSuite();
            case 11:
                return checkYams();
            case 12:
                return sumOfDice();
            default:
                return 0;
        }
    }
}
