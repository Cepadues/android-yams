package com.example.androslacompote;

// IMPORTS
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

// To display data into a recycler view, you need to attach it to an adapter,
// The adapter create a viewHolder that contains the layout and the text fields that will display our data

// CLASS FigureAdapter, created by following John Horton's book,
// Android programming for beginners, chapter 16
// https://univ-scholarvox-com.ezproxy.univ-orleans.fr/book/88912835

public class classFigureAdapter extends RecyclerView.Adapter<classFigureAdapter.classListItemHolder> {

    public int iClicked = -1; // Index of the clicked Item
    private final List<String> mFigureList; // List of string containing the figures' name
    private final List<String> mScoreList; // List of string containing the score of each figure
    private final Context mContext; // Context : main Activity
    public mCallBack listener;
    public boolean iWaiting = false;

    // Interface with the MainActivity class
    public interface mCallBack {
        void onItemClicked();
    }

    public void setOnItemClickListener(mCallBack CB) {
        this.listener = CB;
    }

    // Create a new FigureAdapter from mainActivity, a list of figure and a list of score
    public classFigureAdapter(Context context, List<String> figureList, List<String> scoreList) {
        this.mContext = context;
        this.mFigureList = figureList;
        this.mScoreList = scoreList;
    }

    // Create a new ViewHolder : ListItemHolder
    @NonNull
    @Override
    public classListItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Create the layout contained in figureLayout.xml file
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.figurelayout, parent, false);
        return new classListItemHolder(itemView, this);
    }

    // Set Text of each item to its corresponding value in both List (Score and Figure)
    @Override
    public void onBindViewHolder(classListItemHolder holder, int position) {
        holder.mFigureNameView.setText(this.mFigureList.get(position));
        holder.mFigureScoreView.setText(this.mScoreList.get(position));

        // Change color of the clicked item
        if (position == this.iClicked) {
            // Color of any selected item (purple)
            holder.itemView.setBackgroundColor(Color.parseColor("#BD93F9"));
        } else {
            // Set the color as @color/Selection (same as the recycler view background)
            holder.itemView.setBackgroundColor(Color.parseColor("#44475A"));
        }
    }

    // Allow us to get the number of items in the List
    @Override
    public int getItemCount() {
        return this.mFigureList.size();
    }

    // This class implements our 2 text view (figure name and score)
    // On create it will display the data contained into figure_Name.xml
    // It will also interact with the user on Click
    public class classListItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView mFigureNameView; // Figure Name textView
        public TextView mFigureScoreView; // Figure Score textView
        classFigureAdapter mAdapter;

        // Associate the 2 textView to those defined in the figureLayout.xml
        public classListItemHolder(View view, classFigureAdapter adapter) {
            super(view);
            this.mFigureNameView = view.findViewById(R.id.figureName);
            this.mFigureScoreView = view.findViewById(R.id.figureScore);
            this.mAdapter = adapter;
            view.setOnClickListener(this);
        }

        // Define the onClick action
        @Override
        public void onClick(View view) {
            int iPosition = getLayoutPosition(); // Get item's position
            // Get score at item's position
            String score = classFigureAdapter.this.mScoreList.get(iPosition);

            if (iWaiting) { // Disable the recyclerView click-ability if not needed
                // If figure hasn't been played yet
                if (score.equals("-")) {
                    if (classFigureAdapter.this.iClicked != -1) {
                        // if a figure hasn't been chosen already
                        iPosition = classFigureAdapter.this.iClicked;
                    } else {
                        // then pass the selected figure
                        classFigureAdapter.this.iClicked = iPosition;
                    }
                }
                if (classFigureAdapter.this.listener != null) {
                    classFigureAdapter.this.listener.onItemClicked();
                }
                this.mAdapter.notifyDataSetChanged();
            }
        }
    }
}