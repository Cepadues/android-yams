package com.example.androslacompote;

// Imports
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ToggleButton;
import java.util.ArrayList;

/*
    Since going into landscape mod resets the game, I've made the choice to disable it
*/

public class MainActivity extends AppCompatActivity implements classFigureAdapter.mCallBack {
    private TextView mBonus;
    private TextView mScore;
    private Button mRoll;
    private Button mRollAll;
    private TextView mInfo;
    private TextView mRound;
    private final ToggleButton[] mDiceToggles = new ToggleButton[5];
    private classFigureAdapter mAdapter;

    public YamsDice mJamsDices = new YamsDice();
    public PlayerSheet mPlayer = new PlayerSheet();
    public RecyclerView mRecyclerView;

    private final ArrayList mFigureList = new ArrayList();
    private final ArrayList mScoreList = new ArrayList();

    int Step = -1; // Gameloop step indicator
    int iRound = 1;
    String[] resFiguresName = new String[13];

    public void printDices() {
        for (int i = 0; i < 5; i++) {
            this.mDiceToggles[i].setTextOn(String.valueOf(this.mJamsDices.dice5[i].val_dice));
            this.mDiceToggles[i].setTextOff(String.valueOf(this.mJamsDices.dice5[i].val_dice));
            this.mDiceToggles[i].setChecked(false);
        }
    }

    public void resetDices() { // Print the string in strings.xml for dice1
        for (int i = 0; i < 5; i++) {
            this.mDiceToggles[i].setTextOn(getResources().getText(R.string.dice1_text));
            this.mDiceToggles[i].setTextOff(getResources().getText(R.string.dice1_text));
            this.mDiceToggles[i].setChecked(false);
        }
    }

    public void setInfoText(String text) {
        this.mInfo.setText(text);
    }

    public void setRollState(boolean enabled) {
        if (enabled) {
            this.mRoll.setBackgroundColor(getResources().getColor(R.color.orange));
        } else {
            this.mRoll.setBackgroundColor(getResources().getColor(R.color.selection));
        }
        this.mRoll.setEnabled(enabled);
    }

    public void setRollAllState(boolean enabled) {
        if (enabled) {
            this.mRollAll.setBackgroundColor(getResources().getColor(R.color.yellow));
        } else {
            this.mRollAll.setBackgroundColor(getResources().getColor(R.color.selection));
        }
        this.mRollAll.setEnabled(enabled);
    }

    public void rollAll(View view) {
        mJamsDices.rollYams("12345");
        printDices();
        gameFunction(false);
    }

    public void rollSelect(View view) {
        for (int i = 0; i < 5; i++) {
            if (this.mDiceToggles[i].isChecked()) {
                mJamsDices.rollYams(String.valueOf(i + 1));
            }
        }
        printDices();
        gameFunction(false);
    }

    public void setRound(int r) {
        this.mRound.setText(String.valueOf(r));
    }

    public void setScore() {
        this.mPlayer.updateScoring(this.mAdapter.iClicked, this.mJamsDices.getScore(this.mAdapter.iClicked));
        this.mScoreList.set(this.mAdapter.iClicked, String.valueOf(this.mJamsDices.getScore(this.mAdapter.iClicked)));
        this.mAdapter.notifyDataSetChanged();
        this.mScore.setText(String.valueOf(this.mPlayer.subTotalScore()));
        this.mBonus.setText(String.valueOf(this.mPlayer.bonus()));
    }

    public void init() {
        mBonus = findViewById(R.id.textBonus);
        mScore = findViewById(R.id.textScore);
        mRoll = findViewById(R.id.button_roll_selected);
        mRollAll = findViewById(R.id.button_roll_all);
        mInfo = findViewById(R.id.textInfo);
        mRound = findViewById(R.id.textRound);

        mScore.setText(String.valueOf(this.mPlayer.subTotalScore()));
        mBonus.setText(String.valueOf(this.mPlayer.bonus()));

        // Init of all Dices
        for (int i = 0; i < 5; i++) {
            String tbName = "Dice" + (i + 1);
            int tbID = getResources().getIdentifier(tbName, "id", getPackageName());
            mDiceToggles[i] = findViewById(tbID);
        }

        // on click, the dice's text will be colored in pink
        for (int i = 0; i < 5; i++) {
            this.mDiceToggles[i].setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (isChecked) {
                    buttonView.setTextColor(getResources().getColor(R.color.pink));
                    // Changing the font size messes with the layout
                    // buttonView.setTextSize(24);
                } else {
                    buttonView.setTextColor(getResources().getColor(R.color.background));
                    // buttonView.setTextSize(34);
                }
            });
        }
        // The roll buttons are linked to their function in MainActivity.xml
        // android:onClick="rollAll" for instance
        // But it's not the recommended way of doing it
        setRollState(false);
        resetDices();
        initRecycleView();
    }

    public void initRecycleView() {
        this.resFiguresName = getResources().getStringArray(R.array.figure_Name);
        for (String i : this.resFiguresName) {
            this.mFigureList.add(i);
            this.mScoreList.add("-");
        }
        this.mAdapter = new classFigureAdapter(this, this.mFigureList, this.mScoreList);
        this.mRecyclerView = findViewById(R.id.figureView);
        this.mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        this.mRecyclerView.setAdapter(mAdapter);
        this.mAdapter.setOnItemClickListener(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    @Override
    public void onItemClicked() {
        gameFunction(true); // Call the gameFunction, indicating that a figure has been clicked
    }

    private void gameFunction(boolean clickedFigure) {
        // If the click comes from the Roll buttons : false
        // If the click comes from the figure list : true
        switch (Step) {
            case -1: // Start of a new round
                if (!clickedFigure) { // Block the figureView from interacting with the game function
                    setRollState(true); // Enable the Roll selected button
                    // Display the round incremented at the end of each round (when the score is
                    // calculated)
                    setRound(iRound);
                    setInfoText(getString(R.string.Roll)); // give instructions to player
                    Step = 0; // 1st Roll
                    return;
                }
                return;

            case 0: // 1st Roll
                if (!clickedFigure) {
                    // If the player must pick a figure to continue
                    if (this.mAdapter.iClicked == -1) {
                        setRollAllState(false);
                        setRollState(false);
                        this.mAdapter.iWaiting = true;
                        this.setInfoText(getString(R.string.Figure));
                        Step = 1;
                    } else {
                        // Else it means the player has to roll
                        setRollAllState(true);
                        setRollState(true);
                        setInfoText(getString(R.string.Roll));
                        Step = 2;
                    }
                }
                return;

            case 1: // 2nd Roll
                if (clickedFigure) {
                    this.mAdapter.iWaiting = false;
                    setRollAllState(true);
                    setRollState(true);
                    setInfoText(getString(R.string.Roll));
                    Step = 1;
                    return;
                }

            case 2: // Figure Step
                if (this.mAdapter.iClicked == -1) { // If a figure must be picked
                    setRollAllState(false);
                    setRollState(false);
                    setInfoText(getString(R.string.Figure));
                    Step = 2;
                    return;
                } else if (!clickedFigure) { // Print score
                    setRollState(false);
                    setInfoText(getString(R.string.Start));
                    setScore();
                    this.mAdapter.iClicked = -1; // Reset the index to -1
                    Step = 3;
                    return;
                } else {
                    return;
                }

            case 3: // 3rd roll
                if (!clickedFigure) {
                    iRound++;
                    if ((iRound - 1) < this.resFiguresName.length) {
                        // Reset the loop for next round
                        setRollAllState(true);
                        setRollState(true);
                        setRound(iRound);
                        setInfoText(getString(R.string.Roll));
                        Step = 0;
                        return;
                    }
                    // Case where every figure has already been played
                    setRollState(false);
                    setRound(iRound);
                    setInfoText(getString(R.string.Start));
                    // Reset the game
                    iRound = 0;
                    this.mPlayer.resetScoring();
                    for (int i = 0; i < this.mScoreList.size(); i++) {
                        this.mScoreList.set(i, "-");
                    }
                    this.mAdapter.notifyDataSetChanged();
                    this.mBonus.setText(String.valueOf(this.mPlayer.subTotalScore()));
                    this.mBonus.setText(String.valueOf(this.mPlayer.bonus()));
                    Step = -1;
                    return;
                }
                return;

            default:
                return;
        }
    }
}