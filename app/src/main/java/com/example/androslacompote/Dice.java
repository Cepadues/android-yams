package com.example.androslacompote;

public class Dice {
    private int nb_faces;
    public int val_dice;

    public Dice(int nbf) {
        this.nb_faces = nbf;
        this.roll();
    }

    public Dice() {
        this.nb_faces = 6;
        this.roll();
    }

    public void roll() {
        val_dice = (int) (Math.random() * this.nb_faces) + 1;
    }

    public void printDice() {
        System.out.println(this.val_dice);
    }

    public int getDiceType() {
        return this.nb_faces;
    }

    public int getDiceValue() {
        return this.val_dice;
    }
}
