package com.example.androslacompote;

public class PlayerSheet {
    private int[] scoringArray = new int[13];

    public PlayerSheet() {
        for (int i = 0; i < 13; i++) {
            scoringArray[i] = 0;
        }
    }

    public int subTotalScore() {
        int total = 0;
        for (int i = 0; i < 13; i++){
            total += scoringArray[i];
        }
        return total;
    }

    public int bonus() {
        return subTotalScore() >= 63 ? 35 : 0;
    }

    public int totalScore() {
        return (subTotalScore()+bonus());
    }

    public void updateScoring(int index, int score) {
        this.scoringArray[index] = score;
    }

    public void resetScoring() {
        for (int i = 0; i < 13; i++) {
            scoringArray[i] = 0;
        }
    }
}
